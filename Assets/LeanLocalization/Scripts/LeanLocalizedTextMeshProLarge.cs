﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Lean.Localization
{
	[ExecuteInEditMode]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(TextMeshProUGUI))]
	public class LeanLocalizedTextMeshProLarge : LeanLocalizedBehaviour
	{

		[Tooltip("If PhraseName couldn't be found, this text will be used")]
		public string FallbackText;
		[Tooltip("If font couldn't be found, this font will be used")]
		public TMP_FontAsset FallbackFont;
		[Tooltip("If font size couldn't be found, this font size will be used")]
		public float FallbackFontSize = 0f;
		[Tooltip("If spacing couldn't be found, this spacing will be used")]
		public float FallbackCharacterSpacing = 0f;

		// This gets called every time the translation needs updating
		public override void UpdateTranslation(LeanTranslation translation)
		{
			// Get the SpriteRenderer component attached to this GameObject
			TextMeshProUGUI textMesh = GetComponent<TextMeshProUGUI>();

			// Use translation?
			if (translation != null)
			{
				if (!string.IsNullOrEmpty(translation.Text))
				{
					textMesh.text = translation.Text;
				}

				if(translation.Object2 != null)
				{
					TextAsset txt = translation.Object2 as TextAsset;
					textMesh.text = txt.text;
				}

				if(translation.Object != null)
					textMesh.font = translation.Object as TMP_FontAsset;
				else
					textMesh.font = FallbackFont;

				if (translation.FontSize != 0f)
					textMesh.fontSize = translation.FontSize;
				else
					textMesh.fontSize = FallbackFontSize;

				textMesh.characterSpacing = translation.CharSpacing;
			}
			// Use fallback?
			else
			{
				textMesh.SetText(FallbackText);
				//textMesh.text = FallbackText;
				textMesh.font = FallbackFont;
				textMesh.fontSize = FallbackFontSize;
				textMesh.characterSpacing = FallbackCharacterSpacing;
			}
		}

		protected virtual void Awake()
		{
			TextMeshProUGUI textMesh = null;
			// Should we set FallbackText?
			if (string.IsNullOrEmpty(FallbackText))
			{
				// Get the TextMeshProUGUI component attached to this GameObject
				textMesh = textMesh ?? GetComponent<TextMeshProUGUI>();

				// Copy current text to fallback
				FallbackText = textMesh.text;
			}

			if (FallbackFont == null)
			{
				// Get the TextMeshProUGUI component attached to this GameObject
				textMesh = textMesh ?? GetComponent<TextMeshProUGUI>();

				// Copy current font to fallback
				FallbackFont = textMesh.font;
			}

			if (FallbackFontSize == 0)
			{
				// Get the TextMeshProUGUI component attached to this GameObject
				textMesh = textMesh ?? GetComponent<TextMeshProUGUI>();

				// Copy current font size to fallback
				FallbackFontSize = textMesh.fontSize;
			}
		}
	}
}
