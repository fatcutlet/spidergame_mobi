﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public Slider volumeSlider;

	protected PGSManager _marketAPI;

	private void Start()
    {
        AudioListener.volume = PrefManager.GetVolume();
		if(volumeSlider != null)
		{
			volumeSlider.value = PrefManager.GetVolume();
		}

		//find playmarket api
		GameObject pm = GameObject.FindGameObjectWithTag("PlaymarketAPI");
		if (pm != null)
			_marketAPI = pm.GetComponent<PGSManager>();

		//do authentication
		_marketAPI.Authenticate();
	}


	public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ChangeSoundState(float value)
    {
		AudioListener.volume = value;
		PrefManager.SetVolume(value);
    }
}
