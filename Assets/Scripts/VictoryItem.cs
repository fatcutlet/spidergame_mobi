﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryItem : MonoBehaviour {

    private GameManager _gameManager;
    private Player _player;
    private Animator _animator;

    #region unity messages
    void Start () {
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        _animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            StartCoroutine(Victory());
        }
    }
    #endregion

    #region protected courutines
    protected IEnumerator Victory()
    {
        _animator.SetTrigger("Open");
        _player.OnVictory();
        yield return new WaitForSeconds(1f);
        _gameManager.Victory();
    }
    #endregion


}
