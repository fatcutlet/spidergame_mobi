﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public static class PrefManager  {

    public static int GetStars(string scene)
    {
        return PlayerPrefs.GetInt(scene, 0);
    }

    public static void SetStars(string scene, int stars)
    {
        PlayerPrefs.SetInt(scene, stars);
    }

    public static void UnlockLevel(string scene)
    {
        SetStars(scene, 0);
        Debug.Log("Scene: " + scene + " unlocked");
    }

    public static bool IsUnlocked(string scene)
    {
        return scene == null ? false : PlayerPrefs.HasKey(scene);
    }

    //public static bool IsSoundOn()
    //{
    //    return PlayerPrefs.GetInt("Sound", 1) == 1;
    //}

    //public static void SetSoundState(bool state)
    //{
    //    if (state)
    //        PlayerPrefs.SetInt("Sound", 1);
    //    else
    //        PlayerPrefs.SetInt("Sound", 0);
    //}

    public static float GetVolume()
	{
		return PlayerPrefs.GetFloat("Sound", 1);
	}

	public static void SetVolume(float volume)
	{
		PlayerPrefs.SetFloat("Sound", volume);
	}

	public static bool IsAchivUnlocked(string achivement)
	{
		return PlayerPrefs.GetInt(achivement, 0) > 0;
	}

	public static void UnlockAchiv(string achivement)
	{
		PlayerPrefs.SetInt(achivement, 1);
	}

	public static void Reset()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("All progress was removed");
    }    
}
