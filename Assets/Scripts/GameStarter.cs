﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStarter : MonoBehaviour {

    public string firstLevel;
	
	void Start () {
        if (!PrefManager.IsUnlocked(firstLevel))
        {
            PrefManager.UnlockLevel(firstLevel);
        }
	}
	
}
