﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine;
using System.Collections.Generic;

public class PGSManager : MonoBehaviour
{
	private static PGSManager instance;
	private bool _userAuth = false;

	protected const string LEADERBOARD_ID = "CgkIruy87swEEAIQAg";
	protected string[] _achivments =
	{
		"CgkIruy87swEEAIQAQ"
	};
	protected Dictionary<int, string> _achivMap = new Dictionary<int, string>();

	#region Unity Messages
	void Start()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);

		Init();
		InitAchivments();
	}
	#endregion

	#region Public Methods

	//authenticate the player
	public void Authenticate()
	{
		if (_userAuth)
		{
			Debug.Log("Already autenticaticated");
			return;
		}

		Debug.Log("Start user autentication");

		Social.localUser.Authenticate((bool success) => {
			_userAuth = success;

			if (success)
			{
				((GooglePlayGames.PlayGamesPlatform)Social.Active).SetGravityForPopups(Gravity.BOTTOM);
				Debug.Log("Success authenticate the user");
			}
			else
			{
				Debug.LogWarning("Failed authenticate the user");
			}

		});
	}

	//update player statistics
	public void UpdateStats(int scount)
	{
		Debug.Log("Start updating user statistics");

		if (!_userAuth)
		{
			Debug.LogWarning("User is not autenticated");
			return;
		}

		//unlock achivements
		int a = scount / 5;
		Debug.Log("Start to ulock achivments.\n a = " + a + ", scount = " + scount);
		for (int i = 1; i <= a; i++)
		{
			string achive;
			Debug.Log("Start to search achivments, key =" + (i*5));
			Debug.Log("Achivments Map:");
			foreach (int t in _achivMap.Keys)
			{
				string value = "";
				_achivMap.TryGetValue(t, out value);
				Debug.Log("Key: " + t + " Value: "+ value);
			}

			if (_achivMap.TryGetValue(i * 5,out achive))
			{
				Debug.Log("Achivment found: " + achive);
				if (!PrefManager.IsAchivUnlocked(achive))
				{
					Social.ReportProgress(achive, 100.0f, (bool success) => {
						if (success)
						{
							PrefManager.UnlockAchiv(achive);
							Debug.Log("Achivment Unlocked: " + achive);
						}
						else
							Debug.LogWarning("Failed to unlock achivment: " + achive);
					});
				}
			}
		}

		//update leaderboard
		Social.ReportScore(scount, LEADERBOARD_ID, (bool success) => {
			if (success)
				Debug.Log("Score was posted to leaderboard");
			else
				Debug.LogWarning("Failed to post score to leaderboard");
		});
	}
	#endregion

	//Init Google Play Services
	void Init()
	{
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
		// enables saving game progress.
		//.EnableSavedGames()
		// registers a callback to handle game invitations received while the game is not running.
		//.WithInvitationDelegate(<callback method>)
		// registers a callback for turn based match notifications received while the
		// game is not running.
		//.WithMatchDelegate(<callback method>)
		// requests the email address of the player be available.
		// Will bring up a prompt for consent.
		.RequestEmail()
		// requests a server auth code be generated so it can be passed to an
		//  associated back end server application and exchanged for an OAuth token.
		//.RequestServerAuthCode(false)
		// requests an ID token be generated.  This OAuth token can be used to
		//  identify the player to other services such as Firebase.
		.RequestIdToken()
		.Build();

		PlayGamesPlatform.InitializeInstance(config);
		// recommended for debugging:
		PlayGamesPlatform.DebugLogEnabled = true;
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate();
	}

	void InitAchivments()
	{
		for (int i = 1; i <= _achivments.Length; i++)
		{
			_achivMap.Add(i * 5, _achivments[i-1]);
		}

		Debug.Log("Achivments Map:");
		foreach (int t in _achivMap.Keys)
		{
			string value = "";
			_achivMap.TryGetValue(t, out value);
			Debug.Log("Key: " + t + " Value: " + value);
		}
	}
}
