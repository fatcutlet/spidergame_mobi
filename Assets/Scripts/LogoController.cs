﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoController : MonoBehaviour {

    public string menu;
    public float wait;
    public SceneFader fader;

	void Start () {
        StartCoroutine(LoadMenu());
	}

    protected IEnumerator LoadMenu()
    {
        yield return new WaitForSeconds(wait);

        fader.FadeTo(menu);
    }
	
}
