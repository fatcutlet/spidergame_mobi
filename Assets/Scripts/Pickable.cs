﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickable : MonoBehaviour {

    public int score;
    public AudioClip pickUpSound;
    public GameObject explosion;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Player player = collision.gameObject.GetComponent<Player>();
            player.AddScore(score, pickUpSound);
            Instantiate(explosion, gameObject.transform.position, gameObject.transform.rotation);
            DestroyObject(gameObject);
        }

    }


}
