﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AdManager : MonoBehaviour
{
	private static AdManager instance;

	private RewardBasedVideoAd rewardBasedVideo;
	private bool _needNewAd;

	protected bool _adLoaded;
	public bool adLoaded
	{
		get { return _adLoaded; }
	}

	protected float _interval = 1f;
	protected float _elapsedTime;

	void Start()
	{

		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);

		#if UNITY_ANDROID
				string appId = "ca-app-pub-2153497380967264~7085590596"; 
		#elif UNITY_IPHONE
				string appId = "unused";
#else
				string appId = "unexpected_platform";
#endif

		// Initialize the Google Mobile Ads SDK.
		MobileAds.Initialize(appId);

		// Get singleton reward based video ad reference.
		this.rewardBasedVideo = RewardBasedVideoAd.Instance;

		// Called when an ad request has successfully loaded.
		rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
		// Called when an ad request failed to load.
		rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
		// Called when an ad is shown.
		rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
		// Called when the ad starts to play.
		rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
		// Called when the user should be rewarded for watching a video.
		rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
		// Called when the ad is closed.
		rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
		// Called when the ad click caused the user to leave the application.
		rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

		_needNewAd = true;
		_adLoaded = false;
	}

	private void Update()
	{
		//if (_needNewAd)
		//	RequestRewardedVideo();
	}

	public void ShowAd()
	{
		rewardBasedVideo.Show();
	}

	private void RequestRewardedVideo()
	{
		Debug.Log("Go request ad");
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-2153497380967264/4278575801";
		#elif UNITY_IPHONE
            string adUnitId = "unused";
		#else
            string adUnitId = "unexpected_platform";
		#endif

		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder()
			.AddTestDevice("1572A42B90AD93E70E9A7A8345AA110C")
			.AddTestDevice(AdRequest.TestDeviceSimulator)
			.AddKeyword("game")
			.TagForChildDirectedTreatment(false)
			.AddExtra("color_bg", "9B30FF")
			.Build();
		// Load the rewarded video ad with the request.
		this.rewardBasedVideo.LoadAd(request, adUnitId);
		_needNewAd = false;
		Debug.Log("Go ad requested");
	}

	public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
		Debug.Log("Go ad loaded");
		_adLoaded = true;
	}

	public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		MonoBehaviour.print(
			"HandleRewardBasedVideoFailedToLoad event received with message: "
							 + args.Message);
		Debug.Log("Failed to load ad: " + args.Message);
		_needNewAd = true;
		_adLoaded = false;
	}

	public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
		_adLoaded = false;
	}

	public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
	}

	public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
		_needNewAd = true;
	}

	public void HandleRewardBasedVideoRewarded(object sender, Reward args)
	{
		string type = args.Type;
		double amount = args.Amount;
		MonoBehaviour.print(
			"HandleRewardBasedVideoRewarded event received for "
						+ amount.ToString() + " " + type);
	}

	public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
		_needNewAd = true;
	}


}
